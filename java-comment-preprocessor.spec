%global section		devel

Summary:	The Most Powerful Multi-Pass Java Preprocessor
Name:		java-comment-preprocessor
Version:	6.0.1
Release:	3%{?dist}
License:	ASL 2.0

URL:		https://github.com/raydac/java-comment-preprocessor
Source0:	https://github.com/raydac/java-comment-preprocessor/archive/%{version}.tar.gz

BuildArch:		noarch
BuildRequires:	maven-local
BuildRequires:	exec-maven-plugin
# Test requirements
BuildRequires:	maven-shared-jar
BuildRequires:	ant-testutil
BuildRequires:	maven-verifier
BuildRequires:	mockito

%description
It is the most powerful multi-pass preprocessor for Java
but also it can be used everywhere for text processing
if the destination technology supports Java like comment definitions.

%package javadoc
Summary:	API docs for %{name}

%description javadoc
This package contains the API Documentation for %{name}.

%prep

%autosetup

# remove unpackaged and dangerous deps
%pom_remove_plugin :animal-sniffer-maven-plugin pom.xml
%pom_remove_plugin :maven-shade-plugin pom.xml

# remove any binary libs
find -name "*.jar" -or -name "*.class" | xargs rm -f

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license texts/LICENSE-2.0.txt
%doc texts/readme.txt

%files javadoc -f .mfiles-javadoc
%license texts/LICENSE-2.0.txt

%changelog
* Fri Apr 15 2016 Pavel Kajaba <pkajaba@redhat.com> - 6.0.1-3
- Deleted unused and dangerous dependencies (review rhbz#1297347)

* Thu Apr 14 2016 Pavel Raiskup <praiskup@redhat.com> - 6.0.1-2
- don't require jpackage-utils (review rhbz#1297347)

* Tue Jan 5 2016 Pavel Kajaba <pkajaba@redhat.com> - 6.0.1-1
- Initial creation of java-comment-preprocessor package
